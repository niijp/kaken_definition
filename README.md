﻿# KAKEN公開XML定義 #

* このリポジトリでは、KAKENで公開しているメタデータの資料をご利用頂くことができます。

### KAKEN_API_parameters_document ###

* KAKEN APIのパラメータの資料です

### KAKEN_XML_definition_document ###

* 「KAKEN 研究課題をさがす」で公開しているXMLのXML定義資料です

### KAKEN_researcher_JSON_definition_document ###

* 「KAKEN 研究者をさがす」で公開しているJSONのJSON定義資料です

---

# KAKEN Published XML Definition Document #

* In this repository, you can use documents for the metadata published in KAKEN.

### KAKEN_API_parameters_document ###

* This is the document of parameters for KAKEN API.

### KAKEN_XML_definition_document ###

* This is the latest XML definition document of XML published in KAKEN Grants.

### KAKEN_researcher_JSON_definition_document ###

* This is the latest JSON definition document of JSON published in KAKEN Researcher.